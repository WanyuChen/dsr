# -*- coding: utf-8 -*-

import pickle

from collections import OrderedDict
import sys
import time

import numpy as np
import theano
from theano import config
import theano.tensor as T
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

import prep2
datasets = {'DSR': (prep2.load_data, prep2.prepare_data)}

SEED = 42
np.random.seed(42)

def numpy_floatX(data):
    return np.asarray(data, dtype=config.floatX)

def get_minibatches_idx(n, minibatch_size, shuffle=False):
    idx_list = np.arange(n, dtype="int32")

    if shuffle:
        np.random.shuffle(idx_list)

    minibatches = []
    minibatch_start = 0
    for i in range(n // minibatch_size):
        minibatches.append(idx_list[minibatch_start:
                                    minibatch_start + minibatch_size])
        minibatch_start += minibatch_size

    if minibatch_start != n:
        minibatches.append(idx_list[minibatch_start:])

    return zip(range(len(minibatches)), minibatches)

def get_dataset(name):
    return datasets[name][0], datasets[name][1]

def zipp(params, tparams):
    for kk, vv in params.items():
        tparams[kk].set_value(vv)

def unzip(zipped):
    new_params = OrderedDict()
    for kk, vv in zipped.items():
        new_params[kk] = vv.get_value()
    return new_params

def load_params(path, params):
    pp = np.load(path)
    for kk, vv in params.items():
        if kk not in pp:
            raise Warning('%s is not in the archive' % kk)
        params[kk] = pp[kk]

    return params

def _p(pp, name):
    return '%s_%s' % (pp, name)

def get_layer(name):
    fns = layers[name]
    return fns

def init_weights(shape):
    sigma = np.sqrt(2. / shape[0])
    return numpy_floatX(np.random.randn(*shape) * sigma)

def ortho_weight(ndim):
    W = np.random.randn(ndim, ndim)
    u, s, v = np.linalg.svd(W)
    return u.astype(config.floatX)

def init_tparams(params):
    tparams = OrderedDict()
    for kk, pp in params.items():
        tparams[kk] = theano.shared(params[kk], name=kk)
    return tparams

def init_params(options):
    params = OrderedDict()
    # embedding
    params['Wemb'] = init_weights((options['n_items'], options['dim_proj']))
    # embedding for action
    params['Aemb'] = init_weights((options['action'], options['action']))
    ##gru parametr inital
    params = get_layer(options['encoder'])[0](options,
                                              params,
                                              prefix=options['encoder'])
    # long_preference MLP layers
    params['Wmlp1'] = init_weights((options['dim_proj'], options['hidden_units']))
    params['Bmlp1'] = init_weights((options['hidden_units'],))
    params['Wmlp11'] = init_weights((options['hidden_units'], options['hidden_units']))
    params['Bmlp11'] = init_weights((options['hidden_units'],))
    params['Wmlp2'] = init_weights((options['hidden_units'], options['hidden_units']))
    params['Bmlp2'] = init_weights((options['hidden_units'],))

    #co- attention
    params['W_c'] = init_weights((options['hidden_units'], options['hidden_units']))
    params['W_g'] = init_weights((options['features'], options['hidden_units']))
    params['W_d'] = init_weights((options['features'], options['hidden_units']))
    params['W_t'] = init_weights((options['features'], options['hidden_units']))
    params['w_hg'] = init_weights((options['features'],1))
    params['w_hd'] = init_weights((options['features'],1))
    
    params['bilil'] = init_weights((options['dim_proj'], options['hidden_units']))
    params['bilis'] = init_weights((options['dim_proj'], 2 *options['hidden_units']))

    return params

##init params in GRU
def param_init_gru(options, params, prefix='gru'):
    Wxrz = np.concatenate([init_weights((options['dim_proj'], options['hidden_units'])),
                           init_weights((options['dim_proj'], options['hidden_units'])),
                           init_weights((options['dim_proj'], options['hidden_units']))], axis=1)
    params[_p(prefix, 'Wxrz')] = Wxrz
    
    Arz = np.concatenate([init_weights((options['action'], options['hidden_units'])),
                           init_weights((options['action'], options['hidden_units'])),
                           init_weights((options['action'], options['hidden_units']))], axis=1)
    params[_p(prefix, 'Arz')] = Arz

    Urz = np.concatenate([ortho_weight(options['hidden_units']),
                          ortho_weight(options['hidden_units'])], axis=1)
    params[_p(prefix, 'Urz')] = Urz

    Uh = ortho_weight(options['hidden_units'])
    params[_p(prefix, 'Uh')] = Uh

    b = np.zeros((3 * options['hidden_units'],))
    params[_p(prefix, 'b')] = b.astype(config.floatX)
    return params

def dropout_layer(state_before, use_noise, trng, drop_p=0.5):
    retain = 1. - drop_p
    proj = T.switch(use_noise, (state_before * trng.binomial(state_before.shape,
                                                             p=retain, n=1,
                                                             dtype=state_before.dtype)), state_before * retain)
    return proj

 
def gru_layer(tparams, state_below,action_below, options, prefix='gru', mask=None):
    nsteps = state_below.shape[0]
    if state_below.ndim == 3:
        n_samples = state_below.shape[1]
    else:
        n_samples = 1

    assert mask is not None

    def _slice(_x, n, dim):
        if _x.ndim == 3:
            return _x[:, :, n * dim:(n + 1) * dim]
        return _x[:, n * dim:(n + 1) * dim]

    def _step(m_, x_,a_, h_):
        preact = T.dot(h_, tparams[_p(prefix, 'Urz')])
        preact = preact + x_[:, 0:2 * options['hidden_units']] + a_[:, 0:2 * options['hidden_units']]

        z = T.nnet.hard_sigmoid(_slice(preact, 0, options['hidden_units']))
        r = T.nnet.hard_sigmoid(_slice(preact, 1, options['hidden_units']))
        h = T.tanh(T.dot((h_ * r), tparams[_p(prefix, 'Uh')]) + _slice(x_, 2, options['hidden_units']) + _slice(a_, 2, options['hidden_units']))

        h = (1.0 - z) * h_ + z * h
        h = m_[:, None] * h + (1. - m_)[:, None] * h_

        return h

    state_below = (T.dot(state_below, tparams[_p(prefix, 'Wxrz')]) +
                   tparams[_p(prefix, 'b')])
    action_below = (T.dot(action_below, tparams[_p(prefix, 'Arz')]))

    hidden_units = options['hidden_units']
    
    rval, updates = theano.scan(_step,
                                sequences=[mask, state_below , action_below],
                                outputs_info=T.alloc(numpy_floatX(0.), n_samples, hidden_units),
                                name=_p(prefix, '_layers'),
                                n_steps=nsteps)
    return rval

layers = {'gru': (param_init_gru, gru_layer)}

def long_pre(tparams, history_below, options):

    def _user_history(u_history):
        mlp1 = T.nnet.hard_sigmoid(T.dot(u_history, tparams['Wmlp1']) + tparams['Bmlp1'])##(n_samples, mlp2_hid)
        mlp11 = T.nnet.hard_sigmoid(T.dot(mlp1, tparams['Wmlp11']) + tparams['Bmlp11'])
        mlp2 = T.tanh(T.dot(mlp11, tparams['Wmlp2']) + tparams['Bmlp2'])
        return mlp2
    user_l, _ = theano.scan(_user_history,
                                sequences=history_below)
    return user_l
    

def build_model(tparams, options):
    trng = RandomStreams(SEED)
    use_noise = theano.shared(numpy_floatX(0.))

    x = T.matrix('x', dtype='int64')
    mask = T.matrix('mask', dtype=config.floatX)
    y = T.vector('y', dtype='int64')
    a = T.matrix('a', dtype='int64')
    h = T.matrix('h', dtype='int64')
    mask_h = T.matrix('mask_h', dtype=config.floatX)

    n_timesteps = x.shape[0]
    n_samples = x.shape[1]
    
    n_historsteps = h.shape[0]


    emb = tparams['Wemb'][x.flatten()].reshape([n_timesteps,
                                                n_samples,
                                                options['dim_proj']])
    
    actions = tparams['Aemb'][a.flatten()].reshape([n_timesteps,
                                                n_samples,
                                                options['action']])
    
    historys = tparams['Wemb'][h.flatten()].reshape([n_historsteps,n_samples,                                               
                                                options['dim_proj']])
    
    if options['use_dropout']:
        emb = dropout_layer(emb, use_noise, trng, drop_p=0.25)
        historys = dropout_layer(historys, use_noise, trng, drop_p=0.25)
        
    

    proj = get_layer(options['encoder'])[1](tparams, emb, actions,options,
                                            prefix=options['encoder'],
                                            mask=mask)# 
    last_h = proj[-1]
    projs = T.transpose(proj,axes = [1,0,2])
    user_long = long_pre(tparams, historys, options)
    user_long = T.transpose(user_long,axes = [1,0,2])
    m_h = T.transpose(mask_h,axes = [1,0])
    m = T.transpose(mask,axes = [1,0])

    
    def co_attention(state1,state2,m_h,m,state3):
        state1 = state1.T
        state2 = state2.T
        C = T.tanh(T.dot(T.dot(state1.T, tparams['W_c']),state2))

        temp = T.dot(tparams['W_d'],state2)
        temp1 = temp.T+T.dot(state3,tparams['W_t'].T)

        H_g = T.nnet.hard_sigmoid(T.dot(tparams['W_g'],state1)+T.dot(temp1.T,C.T))
        H_d = T.nnet.hard_sigmoid(temp1.T+T.dot(T.dot(tparams['W_g'],state1),C))

        a_g = T.nnet.softmax((T.dot(tparams['w_hg'].T,H_g))*m_h)*m_h 
        p_a_g = a_g.sum(axis=1)[:, None]
        weight_g = a_g/(p_a_g+1e-6)

        a_d = T.nnet.softmax((T.dot(tparams['w_hd'].T,H_d))*m)*m
        p_a_d = a_d.sum(axis=1)[:, None]
        weight_d = a_d/(p_a_d)

        ll = T.dot(state1,weight_g.T)
        cull = ll.flatten()
        ss = T.dot(state2, weight_d.T)
        cuss = ss.flatten()
        return cuss, cull
    
    [cus, cul], _ = theano.scan(
        fn=co_attention,
        sequences=[user_long, projs,m_h,m,last_h]
        )

    c_us = cus
    c_ul = cul

    projss = T.concatenate([c_us, last_h], axis=1)

    if options['use_dropout']:
        projss = dropout_layer(projss, use_noise, trng, drop_p=0.5)

    ytem_l = T.dot(tparams['Wemb'], tparams['bilil'])
    ytem_s = T.dot(tparams['Wemb'], tparams['bilis'])

    pred_l =T.tanh(T.dot(c_ul, ytem_l.T))
    pred_s =T.tanh(T.dot(projss, ytem_s.T))

    pred = T.nnet.softmax(pred_l * pred_s)

    f_pred_prob = theano.function([x, mask,a, h, mask_h], pred, name='f_pred_prob')
    
    off = 1e-8
    if pred.dtype == 'float16':
        off = 1e-6

    cost = -T.log(pred[T.arange(n_samples), y] + off).mean()

    return use_noise, x, a, mask, y, f_pred_prob, cost, h, mask_h


def adam(loss, all_params, learning_rate=0.001, b1=0.9, b2=0.999, e=1e-8, gamma=1-1e-8):
    updates = OrderedDict()
    all_grads = theano.grad(loss, all_params)
    alpha = learning_rate
    t = theano.shared(np.float32(1))
    b1_t = b1*gamma**(t-1)   #(Decay the first moment running average coefficient)

    for theta_previous, g in zip(all_params, all_grads):
        m_previous = theano.shared(np.zeros(theta_previous.get_value().shape, dtype=config.floatX))
        v_previous = theano.shared(np.zeros(theta_previous.get_value().shape, dtype=config.floatX))

        m = b1_t*m_previous + (1 - b1_t)*g  # (Update biased first moment estimate)
        v = b2*v_previous + (1 - b2)*g**2   # (Update biased second raw moment estimate)
        m_hat = m / (1-b1**t)               # (Compute bias-corrected first moment estimate)
        v_hat = v / (1-b2**t)               # (Compute bias-corrected second raw moment estimate)
        theta = theta_previous - (alpha * m_hat) / (T.sqrt(v_hat) + e) #(Update parameters)

        updates[m_previous] = m
        updates[v_previous] = v
        updates[theta_previous] = theta
    updates[t] = t + 1.

    return updates

def pred_evaluation(f_pred_prob, prepare_data, data, iterator):

    recall = 0.0
    mrr = 0.0
    evalutation_point_count = 0
    recalls=[]
    mrrs=[]

    for _, valid_index in iterator:

        x, a ,mask, y,h,mask_h= prepare_data([data[0][t] for t in valid_index],[data[1][t] for t in valid_index],
                                  np.array(data[2])[valid_index],[data[3][t]for t in valid_index])

        preds = f_pred_prob(x, mask,a,h, mask_h)
        targets = y
        ranks = (preds.T > np.diag(preds.T[targets])).sum(axis=0) + 1
        rank_ok = (ranks <= 15)

        recall += rank_ok.sum()

        mrr += (1.0 / ranks[rank_ok]).sum()

        evalutation_point_count += len(ranks)

    recall = numpy_floatX(recall) / evalutation_point_count
    mrr = numpy_floatX(mrr) / evalutation_point_count
    eval_score = (recall, mrr)

    return eval_score


def train_gru(
    dim_proj=50,# word embeding dimension
    action=4,# action number
    features=50,#feature numbers
    hidden_units=100,  # GRU number of hidden units.
    patience=100,  # Number of epoch to wait before early stop if no progress
    max_epochs=30,  # The maximum number of epoch to run
    dispFreq=100,  # Display to stdout the training progress every N updates
    lrate=0.001,  # Learning rate
    n_items=5691,  # Vocabulary size
    encoder='gru',  # TODO: can be removed must be gru.
    saveto='my_model.npz',  # The best model will be saved there
    is_valid=True,  # Compute the validation error after this number of update.
    is_save=False,  # Save the parameters after every saveFreq updates
    batch_size=512,  # The batch size during training.
    valid_batch_size=512,  # The batch size used for validation/test set.
    dataset='DSR',
    use_dropout=True,  # if False slightly faster, but worst test error
                       # This frequently need a bigger model.
    reload_model=False,  # Path to a saved model we want to start from.
    test_size=-1,  # If >0, we keep only this number of test example.
):

    # Model options
    model_options = locals().copy()
    print("model options", model_options)

    load_data, prepare_data = get_dataset(dataset)

    print('Loading data')
    train, valid, test = load_data()

    print('Building model')

    params = init_params(model_options)

    if reload_model:
        load_params('Best_performance_my_model.npz', params)

    tparams = init_tparams(params)

    (use_noise, x,a, mask,
     y, f_pred_prob,cost,h, mask_h) = build_model(tparams, model_options)

    all_params = list(tparams.values())

    updates = adam(cost, all_params, lrate)

    train_function = theano.function(inputs=[x,a, mask, y,h, mask_h], outputs=cost, updates=updates)

    print('Optimization')

    kf_valid = get_minibatches_idx(len(valid[0]), valid_batch_size)
    kf_test = get_minibatches_idx(len(test[0]), valid_batch_size)

    print("%d train examples" % len(train[0]))
    print("%d valid examples" % len(valid[0]))
    print("%d test examples" % len(test[0]))

    history_errs = []
    history_vali = []
    best_p = None
    bad_count = 0

    uidx = 0  # the number of update done
    estop = False  # early stop

    try:
        for eidx in range(max_epochs):
            start_time = time.time()
            n_samples = 0
            epoch_loss = []

            kf = get_minibatches_idx(len(train[0]), batch_size, shuffle=True)

            for _, train_index in kf:
                uidx += 1
                use_noise.set_value(1.)
                y = [train[2][t] for t in train_index]
                a = [train[1][t] for t in train_index]
                x = [train[0][t]for t in train_index]
                h = [train[3][t]for t in train_index]
                
                x, a, mask, y,h,mask_h = prepare_data(x,a, y,h)
                n_samples += x.shape[1]
            
                
                loss = train_function(x, a,mask, y,h, mask_h)
                epoch_loss.append(loss)

                if np.isnan(loss) or np.isinf(loss):
                    print('bad loss detected: ', loss)
                    return 1., 1., 1.

            print('Epoch ', eidx, 'Update ', uidx, 'Loss ', np.mean(epoch_loss))

            if saveto and is_save:
                print('Saving...')

                if best_p is not None:
                    params = best_p
                else:
                    params = unzip(tparams)
                np.savez(saveto, history_errs=history_errs, **params)
                print('Saving done')

            if is_valid:
                use_noise.set_value(0.)
                
                kf_valid = get_minibatches_idx(len(valid[0]), valid_batch_size)
                kf_test = get_minibatches_idx(len(test[0]), valid_batch_size)
                
                valid_evaluation = pred_evaluation(f_pred_prob, prepare_data, valid, kf_valid)
                test_evaluation = pred_evaluation(f_pred_prob, prepare_data, test, kf_test)
                history_errs.append([valid_evaluation, test_evaluation])

                if best_p is None or valid_evaluation[0] >= np.array(history_vali).max():

                    best_p = unzip(tparams)
                    print('Best perfomance updated!')
                    bad_count = 0

                print('Valid Recall@10:', valid_evaluation[0], '   Valid Mrr@10:', valid_evaluation[1],
                      '\nTest Recall@10', test_evaluation[0], '   Test Mrr@10:', test_evaluation[1])

                if len(history_vali) > 10 and valid_evaluation[0] <= np.array(history_vali).max():
                    bad_count += 1
                    print('===========================>Bad counter: ' + str(bad_count))
                    print('current validation recall: ' + str(valid_evaluation[0]) +
                          '      history max recall:' + str(np.array(history_vali).max()))
                    if bad_count > patience:
                        print('Early Stop!')
                        estop = True

                history_vali.append(valid_evaluation[0])

            end_time = time.time()
            print('Seen %d samples' % n_samples)
            
            if estop:
                break

    except KeyboardInterrupt:
        print("Training interupted")

    if best_p is not None:
        zipp(best_p, tparams)
    else:
        best_p = unzip(tparams)

    use_noise.set_value(0.)
    kf_valid = get_minibatches_idx(len(valid[0]), valid_batch_size)
    kf_test = get_minibatches_idx(len(test[0]), valid_batch_size)
    valid_evaluation = pred_evaluation(f_pred_prob, prepare_data, valid, kf_valid)
    test_evaluation = pred_evaluation(f_pred_prob,  prepare_data, test, kf_test)

    print('=================Best performance=================')
    print('Valid Recall@10:', valid_evaluation[0], '   Valid Mrr@10:', valid_evaluation[1],
          '\nTest Recall@10', test_evaluation[0], '   Test Mrr@10:', test_evaluation[1])
    print('==================================================')
    if saveto and is_save:
        np.savez('Best_performance_my_model', valid_evaluation=valid_evaluation, test_evaluation=test_evaluation, history_errs=history_errs,
                 **best_p)

    return valid_evaluation, test_evaluation

if __name__ == '__main__':
    eval_valid, eval_test= train_gru(max_epochs=1, test_size=-1)






