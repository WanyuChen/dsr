# -*- coding: utf-8 -*-

import time
import csv
import pickle
import numpy
import operator
from collections import OrderedDict

with open("sessions.csv", "rt") as f:
    reader = csv.DictReader(f, delimiter=',')
    sess_clicks = OrderedDict()
    sess_date = OrderedDict()
    user_session = OrderedDict()
    ctr = 0
    curid = -1
    curdate = None
    for data in reader:
        sessid = data['session_id']
        userid = data['user_id']
        if curdate and not curid == sessid:
            date = time.mktime(time.strptime(curdate, '%Y-%m-%d'))
            sess_date[curid] = date
        curid = sessid
        item = data['item_id']
        interaction = data['type']
        curdate = data['time']
        if sessid in sess_clicks:
            sess_clicks[sessid] += [[item,interaction,userid]]
        else:
            sess_clicks[sessid] =[[item,interaction,userid]]
        ctr += 1
        if ctr % 100000 == 0:
            print ('Loaded', ctr)
            
        if userid in user_session:
            if sessid not in user_session[userid]:
                user_session[userid] += [sessid]
        else:
            user_session[userid] = [sessid]
            
    date = time.mktime(time.strptime(curdate, '%Y-%m-%d'))
    sess_date[curid] = date

# Split out test set based on dates
dates = list(sess_date.items())
maxdate = dates[0][1]

for _, date in dates:
    if maxdate < date:
        maxdate = date

# 7 days for test
splitdate = maxdate - 86400 * 7
print('Split date', splitdate)
train_sess = filter(lambda x: x[1] <=splitdate, dates)
test_sess = filter(lambda x: x[1] > splitdate, dates)

# Sort sessions by date
train_sess = sorted(train_sess, key=operator.itemgetter(1))
test_sess = sorted(test_sess, key=operator.itemgetter(1))

item_dict = OrderedDict()
item_ctr = 1

user_dict = OrderedDict()
user_ctr = 1

train_seqs = []
train_dates = []

# Convert training sessions to sequences and renumber items to start from 1
sess_click_copy = OrderedDict()
keys_train = []
train_len=0
for s, date in train_sess:      
    seq = sess_clicks[s]     
    outseq = []
    for i in seq:
        if i[2] in user_dict:
            user_num = user_dict[i[2]]
        else:
            user_num = user_ctr
            user_dict[i[2]]= user_ctr
            user_ctr +=1

        if i[0] in item_dict:
            outseq += [[item_dict[i[0]],i[1],user_num]]
        else:
            outseq += [[item_ctr,i[1],user_num]]
            item_dict[i[0]] = item_ctr
            item_ctr += 1
    if len(outseq) < 2:
        continue
    train_len = train_len+len(outseq)
    keys_train.append(s)
    sess_click_copy[s] = outseq
    train_seqs += [outseq]
    train_dates += [date]
print('behavior_in_train: {}'.format(train_len))
test_seqs = []
test_dates = []
keys_test = []
test_len=0
# Convert test sessions to sequences, ignoring items that do not appear in training set
for s, date in test_sess:
    seq = sess_clicks[s]
    outseq = []
    for i in seq:
        if i[2] in user_dict:
            user_num = user_dict[i[2]]
        else:
            user_num = user_ctr
            user_dict[i[2]]= user_ctr
            user_ctr +=1

        if i[0] in item_dict:
            outseq += [[item_dict[i[0]],i[1],user_num]]
    if len(outseq) < 2:
        continue
    test_len = test_len+len(outseq)
    keys_test.append(s)
    sess_click_copy[s] = outseq
    test_seqs += [outseq]
    test_dates += [date]
    
print('behavior_in_test: {}'.format(test_len))
print('item_ctr: {}'.format(item_ctr))
print('user_ctr: {}'.format(user_ctr))

sess_clicks_train = {key: value for key, value in sess_click_copy.items() if key in keys_train}
sess_clicks_test = {key: value for key, value in sess_click_copy.items() if key in keys_test}
session_len = [len(sess_click_copy[s]) for s in sess_click_copy]
print('maxlen: {}'.format(numpy.max(session_len)))

user_history=OrderedDict()
for user_id in user_session:
    sess0 = user_session[user_id][0]
    user_history[sess0] =[]
    history=[]
    for i in range(1,len(user_session[user_id])):
        pre_sess =  user_session[user_id][i-1]
        sess = user_session[user_id][i]
        try:
            for j in sess_click_copy[pre_sess]:
                if int(j[1]) >= 2:
                    history.append(j[0])
        except:
            pass
        if sess not in user_history:
            user_history[sess]=history.copy()

user_history_train = {key: value for key, value in user_history.items() if key in keys_train}
user_history_test = {key: value for key, value in user_history.items() if key in keys_test}

def process_seqs1(sess_click_seq,user_history_seq):
    out_seqs = []
    out_history = []
    labs = []
    for item in sess_click_seq:
        sess_seq = sess_click_seq[item]
        history_seq = user_history_seq[item]
        for i in range(1, len(sess_seq)):
            tar = sess_seq[-i][0]
            labs += [tar]
            out_seqs += [sess_seq[:-i]]
            out_history += [history_seq]

    return out_seqs, out_history, labs

tr_seqs1, tr_history, tr_labs1 = process_seqs1(sess_clicks_train,user_history_train)
te_seqs1, te_history, te_labs1 = process_seqs1(sess_clicks_test,user_history_test)

tr_seqs1_action = []
tr_seqs1_item = []
tr_seqs1_user = []
for i in range(len(tr_seqs1)):
    items=[]
    actions=[]
    for item in tr_seqs1[i]:
        items += [item[0]]
        actions += [item[1]]
        user = item[2]
    tr_seqs1_item += [items]
    tr_seqs1_action += [actions]
    tr_seqs1_user += [user]


te_seqs1_action = []
te_seqs1_item = []
te_seqs1_user = []
for i in range(len(te_seqs1)):
    items=[]
    actions=[]
    for item in te_seqs1[i]:
        items += [item[0]]
        actions += [item[1]]
        user = item[2]
    te_seqs1_item += [items]
    te_seqs1_action += [actions]
    te_seqs1_user += [user]


train = (tr_seqs1_item,tr_seqs1_action, tr_labs1,tr_history, tr_seqs1_user)
test = (te_seqs1_item,te_seqs1_action, te_labs1,te_history, te_seqs1_user)


f1 = open('train','wb')
pickle.dump(train, f1)
f1.close()
f2 = open('test','wb')
pickle.dump(test, f2)
f2.close()

print('Done.')