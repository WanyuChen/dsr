# -*- coding: utf-8 -*-

from __future__ import print_function
import pickle

import numpy
import theano
numpy.random.seed(42)


def prepare_data(seqs_x,seqs_a,labels,seqs_h):

    lengths = [len(s) for s in seqs_x]
    lengths_h = [len(h) for h in seqs_h]
    
    n_samples = len(seqs_x)
    maxlen = numpy.max(lengths)
    maxlen_h = numpy.max(lengths_h)

    x = numpy.zeros((maxlen, n_samples)).astype('int64')
    a = numpy.zeros((maxlen, n_samples)).astype('int64')
    h = numpy.zeros((maxlen_h, n_samples)).astype('int64')
    x_mask = numpy.ones((maxlen, n_samples)).astype(theano.config.floatX)
    h_mask = numpy.ones((maxlen_h, n_samples)).astype(theano.config.floatX)
    for idx, s in enumerate(seqs_x):
        x[:lengths[idx], idx] = s
    for idx, sa in enumerate(seqs_a):
        a[:lengths[idx], idx] = sa
    for idx, sh in enumerate(seqs_h):
        h[:lengths_h[idx], idx] = sh

    x_mask *= (1 - (x == 0))
    h_mask *= (1 - (h == 0))

    return x,a, x_mask, labels,h,h_mask


def load_data(valid_portion=0.1, maxlen=158, sort_by_len=False):
    path_train_data = 'train'
    path_test_data = 'test'

    f1 = open(path_train_data, 'rb')
    train_set = pickle.load(f1)
    f1.close()

    f2 = open(path_test_data, 'rb')
    test_set = pickle.load(f2)
    f2.close()

    if maxlen:
        new_train_set_x = []
        new_train_set_a = []
        new_train_set_y = []
        new_train_set_z = []
        for x, a, y, z in zip(train_set[0], train_set[1],train_set[2],train_set[3]):## items,actions, labs, historys
            if len(x) < maxlen:
                new_train_set_x.append(x)
                new_train_set_a.append(a)
                new_train_set_y.append(y)
                new_train_set_z.append(z)
            else:
                new_train_set_x.append(x[:maxlen])
                new_train_set_a.append(a[:maxlen])
                new_train_set_y.append(y)
                new_train_set_z.append(z)
        train_set = (new_train_set_x,new_train_set_a, new_train_set_y,new_train_set_z)
        del new_train_set_x,new_train_set_a, new_train_set_y,new_train_set_z

        new_test_set_x = []
        new_test_set_a = []
        new_test_set_y = []
        new_test_set_z = []
        for xx, aa, yy, zz in zip(test_set[0], test_set[1],test_set[2],test_set[3]):
            if len(xx) < maxlen:
                new_test_set_x.append(xx)
                new_test_set_a.append(aa)
                new_test_set_y.append(yy)
                new_test_set_z.append(zz)
            else:
                new_test_set_x.append(xx[:maxlen])
                new_test_set_a.append(aa[:maxlen])
                new_test_set_y.append(yy)
                new_test_set_z.append(zz)
        test_set = (new_test_set_x, new_test_set_a,new_test_set_y,new_test_set_z)
        del new_test_set_x,new_test_set_a, new_test_set_y,new_test_set_z

    # split training set into validation set
    train_set_x, train_set_a, train_set_y,train_set_z = train_set
    n_samples = len(train_set_x)
    sidx = numpy.arange(n_samples, dtype='int32')
    numpy.random.shuffle(sidx)
    n_train = int(numpy.round(n_samples * (1. - valid_portion)))
    valid_set_x = [train_set_x[s] for s in sidx[n_train:]]
    valid_set_a = [train_set_a[s] for s in sidx[n_train:]]
    valid_set_y = [train_set_y[s] for s in sidx[n_train:]]
    valid_set_z = [train_set_z[s] for s in sidx[n_train:]]
    
    train_set_x = [train_set_x[s] for s in sidx[:n_train]]
    train_set_a = [train_set_a[s] for s in sidx[:n_train]]
    train_set_y = [train_set_y[s] for s in sidx[:n_train]]
    train_set_z = [train_set_z[s] for s in sidx[:n_train]]

    train_set = (train_set_x, train_set_a,train_set_y,train_set_z)
    valid_set = (valid_set_x, valid_set_a,valid_set_y,valid_set_z)

    test_set_x, test_set_a, test_set_y, test_set_z = test_set
    valid_set_x,valid_set_a, valid_set_y,valid_set_z = valid_set
    train_set_x,train_set_a, train_set_y,train_set_z = train_set

    def len_argsort(seq):
        return sorted(range(len(seq)), key=lambda x: len(seq[x]))

    if sort_by_len:
        sorted_index = len_argsort(test_set_x)
        test_set_x = [test_set_x[i] for i in sorted_index]
        test_set_a = [test_set_a[i] for i in sorted_index]
        test_set_y = [test_set_y[i] for i in sorted_index]
        test_set_z = [test_set_z[i] for i in sorted_index]

        sorted_index = len_argsort(valid_set_x)
        valid_set_x = [valid_set_x[i] for i in sorted_index]
        valid_set_a = [valid_set_a[i] for i in sorted_index]
        valid_set_y = [valid_set_y[i] for i in sorted_index]
        valid_set_z = [valid_set_z[i] for i in sorted_index]

    train = (train_set_x,  train_set_a,train_set_y,train_set_z)
    valid = (valid_set_x,valid_set_a, valid_set_y,valid_set_z)
    test = (test_set_x,test_set_a, test_set_y,test_set_z)

    return train, valid, test
