import numpy as np
import pandas as pd
import subprocess
import argparse
import os
import datetime

def make_sessions(data, session_th=0, is_ordered=False, user_key='user_id', item_key='item_id', time_key='ts'):
    """Assigns session ids to the events in data without grouping keys"""
    if not is_ordered:
        # sort data by user and time
        data.sort_values(by=[user_key, time_key], ascending=True, inplace=True)
    b = np.array(data[time_key])
    tdiff = np.diff(b)/np.timedelta64(1,'s')
    split_session = tdiff > session_th
    split_session = np.r_[True, split_session]
    new_user = data['user_id'].values[1:] != data['user_id'].values[:-1]
    new_user = np.r_[True, new_user]
    new_session = np.logical_or(new_user, split_session)
    session_ids = np.cumsum(new_session)
    data['session_id'] = session_ids
    return data

alibaba = pd.read_csv('t_alibaba_data.csv', header=0, sep=',')
alibaba.visit_datetime = pd.to_datetime(alibaba.visit_datetime.replace('/','-'))

print('Original data:')
print('Num items: {}'.format(alibaba.brand_id.nunique()))
print('Num users: {}'.format(alibaba.user_id.nunique()))
print('Num interactions: {}'.format(len(alibaba)))
data = make_sessions(alibaba, user_key='user_id', item_key='brand_id', time_key='visit_datetime')
user_key='user_id'
item_key='brand_id'
time_key='visit_datetime'
print('Num sessions: {}'.format(data.session_id.nunique()))


print('Filtering data')
item_pop = data.brand_id.value_counts()
good_items = item_pop[item_pop >= 3].index
inter_dense = data[data.brand_id.isin(good_items)]

session_length = inter_dense.session_id.value_counts()
good_sessions = session_length[session_length >= 3].index
inter_dense = inter_dense[inter_dense.session_id.isin(good_sessions)]

sess_per_user = inter_dense.groupby(user_key)['session_id'].nunique()
good_users = sess_per_user[(sess_per_user >= 1) & (sess_per_user < 200000)].index
inter_dense = inter_dense[inter_dense.user_id.isin(good_users)]

item_per_user = inter_dense.groupby(user_key)[item_key].nunique()
good_users_item = item_per_user[item_per_user >= 3].index
inter_dense = inter_dense[inter_dense.user_id.isin(good_users_item)]

print('Filtered data:')
print('Num items: {}'.format(inter_dense.brand_id.nunique()))
print('Num users: {}'.format(inter_dense.user_id.nunique()))
print('Num sessions: {}'.format(inter_dense.session_id.nunique()))
print('Num behaviors: {}'.format(len(inter_dense)))

behavior_per_user=inter_dense.user_id.value_counts()
print('behavior_per_user: {}'.format(np.mean(behavior_per_user)))
behavior_per_item=inter_dense.brand_id.value_counts()
print('behavior_per_item: {}'.format(np.mean(behavior_per_item)))

session_length = inter_dense.session_id.value_counts()
print('session_length.max')
print(session_length.max())


def remap_columns(data, c):
    """Remap the values in each to the fields in `columns` to the range [0, number of unique values]"""
    uniques = data[c].unique()
    col_map = pd.Series(index=uniques, data=np.arange(len(uniques)))
    data[c] = col_map[data[c]].values    
    return data

inter_dense = remap_columns(inter_dense, 'session_id')

inter_dense.to_csv('sessions.csv', header = ['user_id','item_id','type','time','session_id'], index=False)

    
    